package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name ="asdosLog")
@Data
@Getter
@NoArgsConstructor
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", nullable = false, updatable = false)
    private int id;

    @Column(name = "mulai")
    private LocalDateTime mulai;

    @Column(name = "selesai")
    private LocalDateTime selesai;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "npm")
    private Mahasiswa mahasiswa;


    public Log(String mulai, String selesai, String description) {
        this.mulai = LocalDateTime.parse(mulai);
        this.selesai = LocalDateTime.parse(selesai);
        this.description = description;
    }
}