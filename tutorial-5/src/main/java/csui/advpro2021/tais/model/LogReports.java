package csui.advpro2021.tais.model;

import lombok.Getter;
import lombok.Setter;

import java.time.Month;

@Getter
@Setter
public class LogReports {
    private String bulan;
    private long jamKerja = 0;
    private long pembayaran = 0;

    public LogReports(int bulan) {
        this.bulan = Month.of(bulan).toString();
    }

    public void tambahJam(long jam) {
        this.jamKerja += jam/3600;
    }

    public void finalisasi() {
        setJamKerja(Math.round(this.jamKerja * 100) / 100);
        setPembayaran(this.jamKerja * 350);
    }
}