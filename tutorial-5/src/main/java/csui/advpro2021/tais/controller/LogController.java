package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.service.LogService;
import csui.advpro2021.tais.service.MahasiswaService;
import csui.advpro2021.tais.service.MataKuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;


@RestController
@RequestMapping(path = "/log")
public class LogController {

    @Autowired
    LogService logService;

    @PostMapping(path="/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postLog(@RequestBody Log log, @PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.createLog(log, npm));
    }

    @GetMapping(path = "/{npm}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getAllLog(@PathVariable(value = "npm") String npm) {
        if (logService.getLogsOfMahasiswa(npm) != null) {
            return ResponseEntity.ok(logService.getLogsOfMahasiswa(npm));
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PutMapping(path = "/{npm}/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "npm") String npm, @PathVariable(value = "idLog") String id, @RequestBody Log log) {
        return ResponseEntity.ok(logService.updateLog(log, id));
    }

    @DeleteMapping(path = "/{npm}/{idLog}", produces = {"application/json"})
    @ResponseBody
    @Transactional
    public ResponseEntity deleteLog(@PathVariable(value = "idLog") String id) {
        logService.deleteLog(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


    @GetMapping(path = "/{npm}/pembayaran", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getPembayaran(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getSummaryOfMahasiswa(npm));
    }
}