package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogReports;
import csui.advpro2021.tais.model.Mahasiswa;

import java.util.List;

public interface LogService {
    Log createLog(Log log, String npm);
    Log updateLog(Log log, String id);
    void deleteLog(String id);
    List<Log> getLogsOfMahasiswa(String npm);
    LogReports[] getSummaryOfMahasiswa(String npm);
}
