package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.LogReports;
import csui.advpro2021.tais.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private MahasiswaService mahasiswaService;

    @Autowired
    LogRepository logRepository;

    public Log getLogById(String id) {
        return logRepository.findById(Integer.parseInt(id));
    }

    @Override
    public Log createLog(Log log, String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Log updateLog(Log log, String id) {
        Log tempLog = getLogById(id);
        log.setMahasiswa(tempLog.getMahasiswa());
        log.setId(Integer.parseInt(id));
        logRepository.save(log);
        return tempLog;
    }

    @Override
    public void deleteLog(String id) {
        logRepository.deleteById(Integer.parseInt(id));
    }

    @Override
    public List<Log> getLogsOfMahasiswa(String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa != null) return mahasiswa.getLogs();
        return null;
    }

    @Override
    public LogReports[] getSummaryOfMahasiswa(String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);

        if (mahasiswa != null) {
            LogReports[] logSummary = new LogReports[12];
            for (int i = 0; i < logSummary.length; i++) {
                logSummary[i] = new LogReports(i+1);
            }

            for (Log log: mahasiswa.getLogs()) {
                int bulan = log.getMulai().getMonthValue();
                long durasiKerja = ChronoUnit.SECONDS.between(log.getMulai(), log.getSelesai());
                System.out.println(durasiKerja);
                logSummary[bulan-1].tambahJam(durasiKerja);
            }

            for (LogReports log: logSummary) {
                log.finalisasi();
            }
            return logSummary;
        }
        return null;
    }
}

