package csui.advpro2021.tais.service;


import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.LogReports;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.lenient;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private MahasiswaServiceImpl mahasiswaService;

    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;

    private Log log;

    private Log log2;

    @BeforeEach
    public void setup() {
        mahasiswa = new Mahasiswa();
        log = new Log();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");

        String mulai = "2019-01-04T10:11:30";
        String selesai = "2019-01-04T11:11:30";
        log = new Log(mulai, selesai, "test");
        log.setMahasiswa(mahasiswa);

    }

    @Test
    public void testCreateLog() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);

        lenient().when(logService.createLog(this.log, mahasiswa.getNpm())).thenReturn(this.log);
    }

    @Test
    public void testUpdateLog() throws Exception {
        when(logRepository.findById(log.getId())).thenReturn(log);
        String currDesc = log.getDescription();
        log.setDescription("ini update");

        lenient().when(logService.updateLog(this.log, String.valueOf(log.getId()))).thenReturn(this.log);
        Log logResult = logService.updateLog(this.log, String.valueOf(this.log.getId()));

        assertNotEquals(currDesc, logResult.getDescription());
        assertEquals(logResult.getId(), log.getId());

    }

    @Test
    public void testDeleteLog() throws Exception {
        logService.createLog(log, mahasiswa.getNpm());
        logService.deleteLog(String.valueOf(log.getId()));
        lenient().when(logRepository.findById(log.getId())).thenReturn(null);
        assertEquals(logRepository.findById(log.getId()), null);
    }

    @Test
    public void testGetLogsOfMahasiswaExists() throws Exception {
        assertEquals(logService.getLogsOfMahasiswa(mahasiswa.getNpm()), mahasiswa.getLogs());
    }

    @Test
    public void testGetLogsOfMahasiswaNotExists() throws Exception {
        assertEquals(logService.getLogsOfMahasiswa("100"), null);
    }

    @Test
    public void testGetSummaryOfMahasiswaExists() throws Exception {
        String npm = mahasiswa.getNpm();
        List<Log> logs = new ArrayList<>();
        logs.add(log);
        mahasiswa.setLogs(logs);
        lenient().when(mahasiswaService.getMahasiswaByNPM(npm)).thenReturn(mahasiswa);

        LogReports[] summary = logService.getSummaryOfMahasiswa(mahasiswa.getNpm());
        assertEquals(summary.length, 12);
        assertEquals(summary[0].getBulan(), "JANUARY");
        assertEquals(summary[0].getJamKerja(), 1.0);

    }

    @Test
    public void testGetSummaryOfMahasiswaNotExists() throws Exception {
        LogReports[] summary = logService.getSummaryOfMahasiswa("123");
        assertEquals(summary, null);
    }
}
