package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.LogReports;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.http.MediaType;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.mockito.Mockito.when;


@WebMvcTest(controllers = {LogController.class})
public class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @MockBean
    private MahasiswaRepository mahasiswaRepository;


    private Mahasiswa mahasiswa;

    private Log log;

    @BeforeEach
    public void setup() {
        mahasiswa = new Mahasiswa("1906192052", "Quanta Quantul", "quanta@cs.ui.ac.id", "4",
                "081317691718");
        log = new Log();
        log.setMulai(LocalDateTime.parse("2021-04-06T10:00:00"));
        log.setSelesai(LocalDateTime.parse("2019-08-04T11:00:00"));
        log.setDescription("ngetest");
    }

    private String mapperLogToJson(String desc) throws JsonProcessingException {
        return String.format("{\"start\": \"2021-04-06T10:00:00\", \"end\": \"2019-08-04T11:00:00\", \"description\": \"%s\"}", desc);
    }

    @Test
    public void testPostLog() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(mahasiswa);
        when(logService.createLog(ArgumentMatchers.any(Log.class), ArgumentMatchers.any(String.class)))
                .thenReturn(this.log);
        mahasiswaService.createMahasiswa(mahasiswa);


        mvc.perform(post("/log/1906192052").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapperLogToJson("testing")))
                .andExpect(jsonPath("$.description").value("ngetest"));
    }

    @Test
    public void testGetAllLogExists() throws Exception {
        ArrayList<Log> logs = new ArrayList<>();
        logs.add(log);
        when(logService.getLogsOfMahasiswa("1906192052")).thenReturn(logs);

        mvc.perform(get("/log/1906192052")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$[0].description").value("ngetest"));
    }

    @Test
    public void testGetAllLogNotExists() throws Exception {
        when(logService.getLogsOfMahasiswa("123456")).thenReturn(null);
        mvc.perform(get("/log/123456")).andExpect(status().isNoContent());
    }

    @Test
    public void testUpdateLog() throws Exception {
        log.setDescription("testing update");
        when(logService.updateLog(ArgumentMatchers.any(Log.class), anyString())).thenReturn(log);

        mvc.perform(put("/log/1906192052/0")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapperLogToJson("testing update")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.description").value("testing update"));
    }

    @Test
    public void testDeleteLog() throws Exception {
        when(logService.createLog(log, "1906192052")).thenReturn(log);
        logService.createLog(log, "1906192052");
        int id = log.getId();
        mvc.perform(delete("/log/1906192052/" + id)).andExpect(status().isNoContent());
    }

    @Test
    public void testGetPembayaran() throws Exception {
        when(mahasiswaRepository.findByNpm("1906192052")).thenReturn(mahasiswa);
        mahasiswaService.createMahasiswa(mahasiswa);

        LogReports[] logSummary = new LogReports[12];
        logSummary[3] = new LogReports(3);
        logSummary[3].setJamKerja(1);
        logSummary[3].finalisasi();
        when(logService.getSummaryOfMahasiswa(anyString())).thenReturn(logSummary);

        mvc.perform(get("/log/1906192052/pembayaran")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$[3].jamKerja").value(1));
    }
}
