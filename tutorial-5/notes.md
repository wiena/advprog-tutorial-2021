##Requirement TAIS
- Mahasiswa mendaftar ke satu mata kuliah dan langsung diterima
- Mata Kuliah yang sudah ada asisten tidak dapat dihapus.
- Mahasiswa yang sudah terdaftar menjadi asisten tidak bisa dihapus.
- Mahasiswa dapat create, update, delete log.
- API digunakan untuk melaporkan laporan pembayaran yang terdiri dari jumlah jam kerja dan jumlah uang yang didapat asisten pada tiap bulan.
