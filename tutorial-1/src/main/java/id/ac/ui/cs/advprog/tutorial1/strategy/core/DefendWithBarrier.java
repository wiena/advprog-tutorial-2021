package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //ToDo: Complete me
    public String getType() {
        return "Defend with Barrier";
    }

    public String defend() {
        return "barier";
    }
}