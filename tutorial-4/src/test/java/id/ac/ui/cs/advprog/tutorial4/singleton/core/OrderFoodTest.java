package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

public class OrderFoodTest {

    @Mock
    OrderFood orderFood;

    public void setup() throws Exception {
        orderFood = OrderFood.getInstance();
    }

    @Test
    public void orderFoodIsSingleton() {
        OrderFood orderFood1 = OrderFood.getInstance();
        OrderFood orderFood2 = OrderFood.getInstance();
        assertSame(orderFood1, orderFood2);
    }
}
