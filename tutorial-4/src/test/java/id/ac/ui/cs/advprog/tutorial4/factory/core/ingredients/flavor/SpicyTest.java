package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SpicyTest {
    Spicy spicy;

    @BeforeEach
    public void setup() throws Exception {
        spicy = new Spicy();
    }

    @Test
    public void testOverrideGetDescription() {
        assertEquals(spicy.getDescription(), "Adding Liyuan Chili Powder...");
    }
}

