package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class MenuServiceTest {

    private Class<?> menuServiceClass;

    @Mock
    MenuRepository menuRepository;

    private MenuServiceImpl menuServiceImpl;
    private List<Menu> list;

    @BeforeEach
    public void setup() throws Exception {
        menuServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
        menuRepository = new MenuRepository();
        menuServiceImpl = new MenuServiceImpl(menuRepository);
        list = new ArrayList<>();
        list.add(new LiyuanSoba("testing"));
    }

    @Test
    public void testMenuServiceHasGetMenusMethod() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("getMenus");
        int methodModifiers = getMenus.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

        assertEquals(getMenus.getParameterCount(), 0);
    }

    @Test
    public void testCreateMenu() {
        Menu newMenu = menuServiceImpl.createMenu("testing","InuzumaRamen");
        assertEquals(newMenu.getName(), "testing");
        assertThat(newMenu).isInstanceOf(InuzumaRamen.class);
        assertEquals(menuRepository.getMenus().size(), 5);
    }

    @Test
    public void testGetMenu() {
        ReflectionTestUtils.setField(menuRepository, "list", list);
        assertEquals(menuServiceImpl.getMenus(), list);
    }

    @Test
    public void testEmptyArgumentConstructor() {
        MenuServiceImpl newService = new MenuServiceImpl();
        ReflectionTestUtils.setField(newService, "repo", menuRepository);
        ReflectionTestUtils.setField(menuRepository, "list", list);
        assertEquals(newService.getMenus(), list);
    }
}
