package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SweetTest {
    Sweet sweet;

    @BeforeEach
    public void setup() throws Exception {
        sweet = new Sweet();
    }

    @Test
    public void testOverrideGetDescription() {
        assertEquals(sweet.getDescription(), "Adding a dash of Sweet Soy Sauce...");
    }
}

