package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MenuRepositoryTest {

    private MenuRepository menuRepository;

    private Menu sampleMenu;

    @Mock
    private List<Menu> list;


    @BeforeEach
    public void setup() throws Exception {
        menuRepository = new MenuRepository();
        sampleMenu = new InuzumaRamen("testing");
        list = new ArrayList<>();
        list.add(sampleMenu);
    }

    @Test
    public void testGetMenu() {
        ReflectionTestUtils.setField(menuRepository,"list", list);
        List<Menu> mockedList = menuRepository.getMenus();

        assertThat(list).isEqualTo(mockedList);
    }

    @Test
    public void testAdd() {
        ReflectionTestUtils.setField(menuRepository,"list", list);
        LiyuanSoba liyuanSoba = new LiyuanSoba("testing");
        Menu returnedMenu = menuRepository.add(liyuanSoba);
        assertEquals(list.size(), 2);
        assertEquals(returnedMenu, liyuanSoba);
    }




}
