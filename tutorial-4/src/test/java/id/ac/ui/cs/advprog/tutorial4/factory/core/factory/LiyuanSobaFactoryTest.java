package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaFactoryTest {

    private Class<?> liyuanSobaFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        liyuanSobaFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaFactory");
    }

    @Test
    public void testLiyuanSobaFactoryIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(liyuanSobaFactoryClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryIsAIngredientsFactory() {
        Collection<Type> interfaces = Arrays.asList(liyuanSobaFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory")));
    }

    @Test
    public void testOverrideMethodCreateFlavor() {
        IngredientsFactory liyuanSoba = new LiyuanSobaFactory();
        assertThat(liyuanSoba.createFlavor()).isInstanceOf(Sweet.class);
    }

    @Test
    public void testOverrideMethodCreateMeat() {
        IngredientsFactory liyuanSoba = new LiyuanSobaFactory();
        assertThat(liyuanSoba.createMeat()).isInstanceOf(Beef.class);
    }

    @Test
    public void testOverrideMethodCreateNoodle() {
        IngredientsFactory liyuanSoba = new LiyuanSobaFactory();
        assertThat(liyuanSoba.createNoodle()).isInstanceOf(Soba.class);
    }

    @Test
    public void testOverrideMethodCreateTopping() {
        IngredientsFactory liyuanSoba = new LiyuanSobaFactory();
        assertThat(liyuanSoba.createTopping()).isInstanceOf(Mushroom.class);
    }
}
