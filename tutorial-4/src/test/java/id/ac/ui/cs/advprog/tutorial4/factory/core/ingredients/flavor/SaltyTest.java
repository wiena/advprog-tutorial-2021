package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SaltyTest {
    Salty salty;

    @BeforeEach
    public void setup() throws Exception {
        salty = new Salty();
    }

    @Test
    public void testOverrideGetDescription() {
        assertEquals(salty.getDescription(), "Adding a pinch of salt...");
    }
}
