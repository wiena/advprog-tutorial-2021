package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChickenTest {

    Chicken dummy;

    @BeforeEach
    public void setup() throws Exception {
        dummy = new Chicken();
    }

    @Test
    public void testOverrideGetDescription() {
        assertEquals(dummy.getDescription(), "Adding Wintervale Chicken Meat...");
    }
}
