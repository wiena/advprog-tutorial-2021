package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiFactoryTest {

    private Class<?> snevnezhaShiaratakiFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        snevnezhaShiaratakiFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiFactory");
    }

    @Test
    public void testSnevnezhaFactoryIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(snevnezhaShiaratakiFactoryClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaFactoryIsAIngredientsFactory() {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiaratakiFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory")));
    }

    @Test
    public void testOverrideMethodCreateFlavor() {
        IngredientsFactory snevnezhaShirataki = new SnevnezhaShiratakiFactory();
        assertThat(snevnezhaShirataki.createFlavor()).isInstanceOf(Umami.class);
    }

    @Test
    public void testOverrideMethodCreateMeat() {
        IngredientsFactory snevnezhaShirataki = new SnevnezhaShiratakiFactory();
        assertThat(snevnezhaShirataki.createMeat()).isInstanceOf(Fish.class);
    }

    @Test
    public void testOverrideMethodCreateNoodle() {
        IngredientsFactory snevnezhaShirataki = new SnevnezhaShiratakiFactory();
        assertThat(snevnezhaShirataki.createNoodle()).isInstanceOf(Shirataki.class);
    }

    @Test
    public void testOverrideMethodCreateTopping() {
        IngredientsFactory snevnezhaShirataki = new SnevnezhaShiratakiFactory();
        assertThat(snevnezhaShirataki.createTopping()).isInstanceOf(Flower.class);
    }
}
