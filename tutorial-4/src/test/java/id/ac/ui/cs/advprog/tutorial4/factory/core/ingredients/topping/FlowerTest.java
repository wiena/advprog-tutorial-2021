package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FlowerTest {
    Flower dummy;

    @BeforeEach
    public void setup() throws Exception {
        dummy = new Flower();
    }

    @Test
    public void testOverrideGetDescription() {
        assertEquals(dummy.getDescription(), "Adding Xinqin Flower Topping...");
    }
}
