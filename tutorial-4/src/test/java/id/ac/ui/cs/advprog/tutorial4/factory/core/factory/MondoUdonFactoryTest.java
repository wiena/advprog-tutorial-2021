package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonFactoryTest {

    private Class<?> mondoUdonFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        mondoUdonFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonFactory");
    }

    @Test
    public void testMondoUdonFactoryIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(mondoUdonFactoryClass.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryIsAIngredientsFactory() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory")));
    }

    @Test
    public void testOverrideMethodCreateFlavor() {
        IngredientsFactory mondoUdon = new MondoUdonFactory();
        assertThat(mondoUdon.createFlavor()).isInstanceOf(Salty.class);
    }

    @Test
    public void testOverrideMethodCreateMeat() {
        IngredientsFactory mondoUdon = new MondoUdonFactory();
        assertThat(mondoUdon.createMeat()).isInstanceOf(Chicken.class);
    }

    @Test
    public void testOverrideMethodCreateNoodle() {
        IngredientsFactory mondoUdon = new MondoUdonFactory();
        assertThat(mondoUdon.createNoodle()).isInstanceOf(Udon.class);
    }

    @Test
    public void testOverrideMethodCreateTopping() {
        IngredientsFactory mondoUdon = new MondoUdonFactory();
        assertThat(mondoUdon.createTopping()).isInstanceOf(Cheese.class);
    }
}
