package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;



public class LiyuanSobaTest {

    LiyuanSoba liyuanSoba;

    @Mock
    Menu menu;

    @BeforeEach
    public void setup() throws Exception {
        liyuanSoba = new LiyuanSoba("testing");
        menu = mock(Menu.class);
    }


    @Test
    public void testGetName() {
        assertEquals(liyuanSoba.getName(), "testing");
    }


    @Test
    public void testOverrideMethodGetMeat() {
        assertThat(liyuanSoba.getMeat()).isInstanceOf(Beef.class);
    }

    @Test
    public void testOverrideMethodGetNoodle() {
        assertThat(liyuanSoba.getNoodle()).isInstanceOf(Soba.class);
    }

    @Test
    public void testOverrideMethodGetTopping() {
        assertThat(liyuanSoba.getTopping()).isInstanceOf(Mushroom.class);
    }

    @Test
    public void testOverrideMethodGetFlavor() {
        assertThat(liyuanSoba.getFlavor()).isInstanceOf(Sweet.class);
    }

}
