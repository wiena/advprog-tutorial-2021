package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrderServiceTest {

    private Class<?> orderServiceClass;

    private OrderServiceImpl orderServiceImpl;


    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
        orderServiceImpl = new OrderServiceImpl();
    }

    @Test
    public void testMenuServiceHasOrderADrinkMethod() throws Exception {
        Class[] param = new Class[1];
        param[0] = String.class;
        Method orderADrink = orderServiceClass.getDeclaredMethod("orderADrink", param);
        int methodModifiers = orderADrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

        assertEquals(orderADrink.getParameterCount(), 1);
    }

    @Test
    public void testMenuServiceHasOrderAFoodMethod() throws Exception {
        Class[] param = new Class[1];
        param[0] = String.class;
        Method orderAFood = orderServiceClass.getDeclaredMethod("orderAFood", param);
        int methodModifiers = orderAFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

        assertEquals(orderAFood.getParameterCount(), 1);
    }

    @Test
    public void testMenuServiceHasGetFoodMethod() throws Exception {
        Method getMenus = orderServiceClass.getDeclaredMethod("getFood");
        int methodModifiers = getMenus.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

        assertEquals(getMenus.getParameterCount(), 0);
    }

    @Test
    public void testMenuServiceHasGetDrinkMethod() throws Exception {
        Method getMenus = orderServiceClass.getDeclaredMethod("getDrink");
        int methodModifiers = getMenus.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

        assertEquals(getMenus.getParameterCount(), 0);
    }


    @Test
    public void testOrderingADrink() {
        orderServiceImpl.orderADrink("testing order drink");
        OrderDrink dummy = orderServiceImpl.getDrink();
        assertEquals(dummy.toString(), "testing order drink");
    }

    @Test
    public void testOrderingAFood() {
        orderServiceImpl.orderAFood("testing order food");
        OrderFood dummy = orderServiceImpl.getFood();
        assertEquals(dummy.toString(), "testing order food");
    }
}
