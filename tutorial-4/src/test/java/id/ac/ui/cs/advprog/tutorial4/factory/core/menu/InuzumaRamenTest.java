package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class InuzumaRamenTest {

    InuzumaRamen inuzumaRamen;


    @BeforeEach
    public void setup() throws Exception {
        inuzumaRamen = new InuzumaRamen("testing");
    }


    @Test
    public void testGetName() {
        assertEquals(inuzumaRamen.getName(), "testing");
    }


    @Test
    public void testOverrideMethodGetMeat() {
        assertThat(inuzumaRamen.getMeat()).isInstanceOf(Pork.class);
    }

    @Test
    public void testOverrideMethodGetNoodle() {
        assertThat(inuzumaRamen.getNoodle()).isInstanceOf(Ramen.class);
    }

    @Test
    public void testOverrideMethodGetTopping() {
        assertThat(inuzumaRamen.getTopping()).isInstanceOf(BoiledEgg.class);
    }

    @Test
    public void testOverrideMethodGetFlavor() {
        assertThat(inuzumaRamen.getFlavor()).isInstanceOf(Spicy.class);
    }

}
