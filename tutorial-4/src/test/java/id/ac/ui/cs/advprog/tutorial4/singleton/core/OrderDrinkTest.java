package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertSame;

public class OrderDrinkTest {

    @Test
    public void orderDrinkIsSingleton() {
        OrderDrink orderDrink1 = OrderDrink.getInstance();
        OrderDrink orderDrink2 = OrderDrink.getInstance();
        assertSame(orderDrink1, orderDrink2);
    }
}