package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class SnevnezhaShiratakiTest {

    SnevnezhaShirataki snevnezha;

    @BeforeEach
    public void setup() throws Exception {
        snevnezha = new SnevnezhaShirataki("testing");
    }


    @Test
    public void testGetName() {
        assertEquals(snevnezha.getName(), "testing");
    }


    @Test
    public void testOverrideMethodGetMeat() {
        assertThat(snevnezha.getMeat()).isInstanceOf(Fish.class);
    }

    @Test
    public void testOverrideMethodGetNoodle() {
        assertThat(snevnezha.getNoodle()).isInstanceOf(Shirataki.class);
    }

    @Test
    public void testOverrideMethodGetTopping() {
        assertThat(snevnezha.getTopping()).isInstanceOf(Flower.class);
    }

    @Test
    public void testOverrideMethodGetFlavor() {
        assertThat(snevnezha.getFlavor()).isInstanceOf(Umami.class);
    }

}
