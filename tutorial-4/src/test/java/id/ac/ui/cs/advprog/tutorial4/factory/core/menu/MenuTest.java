package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MenuTest {

    Class<?> menuClass;
    Menu menu;

    @Mock
    private IngredientsFactory ingredientsFactory;

    @BeforeEach
    public void setup() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
        ingredientsFactory = mock(IngredientsFactory.class);
        menu = new Menu("testing", ingredientsFactory);
    }

    @Test
    public void testGetName() {
        assertEquals("testing", menu.getName());
    }

    @Test
    public void testHasGetMeat() throws Exception {
        Method getMeat = menuClass.getDeclaredMethod("getMeat");
        int methodModifiers = getMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getMeat.getGenericReturnType().getTypeName());
        assertEquals(0, getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));

    }

    @Test
    public void testHasGetNoodle() throws Exception {
        Method getNoodle = menuClass.getDeclaredMethod("getNoodle");
        int methodModifiers = getNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, getNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testGetTopping() throws Exception {
        Method getTopping = menuClass.getDeclaredMethod("getTopping");
        int methodModifiers = getTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getTopping.getGenericReturnType().getTypeName());
        assertEquals(0, getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testGetFlavor() throws Exception {
        Method getFlavor = menuClass.getDeclaredMethod("getFlavor");
        int methodModifiers = getFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, getFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInitIngredients() {
        Noodle noodle = new Ramen();
        Flavor flavor = new Spicy();
        Meat meat = new Pork();
        Topping topping = new BoiledEgg();
        when(ingredientsFactory.createNoodle()).thenReturn(noodle);
        when(ingredientsFactory.createFlavor()).thenReturn(flavor);
        when(ingredientsFactory.createMeat()).thenReturn(meat);
        when(ingredientsFactory.createTopping()).thenReturn(topping);

        menu.cook();

        assertThat(menu.getNoodle()).isInstanceOf(Ramen.class);
        assertThat(menu.getFlavor()).isInstanceOf(Spicy.class);
        assertThat(menu.getMeat()).isInstanceOf(Pork.class);
        assertThat(menu.getTopping()).isInstanceOf(BoiledEgg.class);

    }
}
