package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SobaTest {
    Soba dummy;

    @BeforeEach
    public void setup() throws Exception {
        dummy = new Soba();
    }

    @Test
    public void testOverrideGetDescription() {
        assertEquals(dummy.getDescription(), "Adding Liyuan Soba Noodles...");
    }

}
