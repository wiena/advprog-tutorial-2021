package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheeseTest {
    Cheese dummy;

    @BeforeEach
    public void setup() throws Exception {
        dummy = new Cheese();
    }

    @Test
    public void testOverrideGetDescription() {
        assertEquals(dummy.getDescription(), "Adding Shredded Cheese Topping...");
    }
}
