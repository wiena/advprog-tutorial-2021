package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenFactoryTest {

    private Class<?> inuzumaRamenFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        inuzumaRamenFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenFactory");
    }

    @Test
    public void testInuzumaRamenFactoryIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(inuzumaRamenFactoryClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryIsAIngredientsFactory() {
        Collection<Type> interfaces = Arrays.asList(inuzumaRamenFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientsFactory")));
    }

    @Test
    public void testOverrideMethodCreateFlavor() {
        IngredientsFactory inuzumaRamen = new InuzumaRamenFactory();
        assertThat(inuzumaRamen.createFlavor()).isInstanceOf(Spicy.class);
    }

    @Test
    public void testOverrideMethodCreateMeat() {
        IngredientsFactory inuzumaRamen = new InuzumaRamenFactory();
        assertThat(inuzumaRamen.createMeat()).isInstanceOf(Pork.class);
    }

    @Test
    public void testOverrideMethodCreateNoodle() {
        IngredientsFactory inuzumaRamen = new InuzumaRamenFactory();
        assertThat(inuzumaRamen.createNoodle()).isInstanceOf(Ramen.class);
    }

    @Test
    public void testOverrideMethodCreateTopping() {
        IngredientsFactory inuzumaRamen = new InuzumaRamenFactory();
        assertThat(inuzumaRamen.createTopping()).isInstanceOf(BoiledEgg.class);
    }
}
