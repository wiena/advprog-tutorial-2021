package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class MondoUdonTest {

    MondoUdon mondoUdon;

    @BeforeEach
    public void setup() throws Exception {
        mondoUdon = new MondoUdon("testing");
    }


    @Test
    public void testGetName() {
        assertEquals(mondoUdon.getName(), "testing");
    }


    @Test
    public void testOverrideMethodGetMeat() {
        assertThat(mondoUdon.getMeat()).isInstanceOf(Chicken.class);
    }

    @Test
    public void testOverrideMethodGetNoodle() {
        assertThat(mondoUdon.getNoodle()).isInstanceOf(Udon.class);
    }

    @Test
    public void testOverrideMethodGetTopping() {
        assertThat(mondoUdon.getTopping()).isInstanceOf(Cheese.class);
    }

    @Test
    public void testOverrideMethodGetFlavor() {
        assertThat(mondoUdon.getFlavor()).isInstanceOf(Salty.class);
    }

}
