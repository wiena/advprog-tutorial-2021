package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FishTest {
    Fish dummy;
    @BeforeEach
    public void setup() throws Exception {
        dummy = new Fish();
    }

    @Test
    public void testOverrideGetDescription() {
        assertEquals(dummy.getDescription(), "Adding Zhangyun Salmon Fish Meat...");
    }
}
