package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UdonTest {
    Udon dummy;

    @BeforeEach
    public void setup() throws Exception {
        dummy = new Udon();
    }

    @Test
    public void testOverrideGetDescription() {
        assertEquals(dummy.getDescription(), "Adding Mondo Udon Noodles...");
    }
}
