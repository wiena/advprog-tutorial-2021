#### Eager instantion
<i>Eager instantiation</i> adalah pattern dimana objek singleton tersebut dibuat ketika kita membuat classnya. Metode ini merupakan thread safe, tidak peduli berapa banyak thread yang mencoba, hasil yang direturn tetaplah objek yang sudah kita buat ketika pertama kali dijalankan. Oleh karena itu tidak ada kemungkinan bahwa dua objek akan dibuat. Kekurangannya, boros speed atau memory penalty ketika loading atau menyimpannya karena objek tetap dibuat meskipun tidak dibutuhkan. 

#### Lazy instantion
<i>Lazy instantiation</i> adalah pattern dimana objek dari singleton pattern ini hanya ketika objek tersebut dibutuhkan. Keuntungannya, tidak boros speed atau memory penalty ketika loading atau menyimpannya karena objek hanya dibuat ketika dibutuhkan. Metode ini tidak thread safe karena dapat terjadi masalah ketika menggunakan di multithread.
