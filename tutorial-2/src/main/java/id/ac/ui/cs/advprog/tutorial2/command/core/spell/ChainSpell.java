package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> chains;

    public ChainSpell(ArrayList<Spell> chains) {
        this.chains = chains;
    }

    @Override
    public void cast() {
        for (Spell spell : chains) {
            spell.cast();
        }
    }
    @Override
    public void undo() {
        // TODO Auto-generated method stub
        for (int i = chains.size() - 1; i >= 0; i--) {
            chains.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
