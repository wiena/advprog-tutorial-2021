package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation {
    private int key;


    public CaesarTransformation(int key){
        this.key = key;
    }

    public CaesarTransformation(){
        this(5);
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? 1 : -1;
        int n = text.length();
        char[] res = new char[n];

        for(int i = 0; i < n; i++) {
            char oldChar = text.charAt(i);
            int newIdx = codex.getIndex(oldChar) + (key*selector);
            newIdx = mod(newIdx,codex.getCharSize());
            res[i] = codex.getChar(newIdx);
        }
        return new Spell(new String(res), codex);


    }

    private int mod(int num1, int num2) {
        int num3 = num1 % num2;
        int res = (num3 < 0) ? (num3+num2) :num3;
        return res;
    }
}