package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class BowAdapter implements Weapon {
    private Bow bow;
    private boolean isAimShoot;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.isAimShoot = true;
    }


    @Override
    public String normalAttack() {
        return bow.shootArrow(this.isAimShoot);
    }

    @Override
    public String chargedAttack() {
        if(this.isAimShoot) {
            this.isAimShoot = false;
            return "Enter aim shot mode";
        } else {
            this.isAimShoot = true;
            return "Leave aim shot mode";
        }
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return bow.getHolderName();
    }
}
