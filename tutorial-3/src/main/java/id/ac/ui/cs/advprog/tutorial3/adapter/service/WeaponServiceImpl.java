package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {
    private List<Weapon> listOfWeapons = new ArrayList<>();

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private BowRepository bowRepository;



    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        if(listOfWeapons.isEmpty()) {
            listOfWeapons.addAll(weaponRepository.findAll());
            for (Spellbook spellBooks: spellbookRepository.findAll()) {
                SpellbookAdapter spellBook = new SpellbookAdapter(spellBooks);
                Weapon weapons = (Weapon) spellBook;
                weaponRepository.save(weapons);
                listOfWeapons.add(weapons);
            }
            for (Bow bows: bowRepository.findAll()) {
                BowAdapter bow = new BowAdapter(bows);
                Weapon weapons = (Weapon) bow;
                weaponRepository.save(weapons);
                listOfWeapons.add(weapons);
            }
        }
        return listOfWeapons;
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        weaponRepository.save(weapon);
        String result = "";
        if(attackType == 1) {
            result += weapon.getHolderName() + " attacked with " + weapon.getName() + "(normal attack) : " + weapon.normalAttack();
        }
        else if(attackType == 2){
            result += weapon.getHolderName() + " attacked with " + weapon.getName() + "(charged attack) : " + weapon.chargedAttack();
        }
        logRepository.addLog(result);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
